import Vue from 'vue'
import myApp from './app.vue'
import vueHeadful from 'vue-headful';

Vue.component('vue-headful', vueHeadful);
Vue.config.productionTip = false;

new Vue({
  render: h => h(myApp),
}).$mount('#sp-App')




